package com.fsd.spring.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.spring.mvc.core.base.ResponseResult;
import com.fsd.spring.mvc.entity.LabUser;
import com.fsd.spring.mvc.service.UserService;

@RestController
@RequestMapping({"/login"})

class LoginController {
	
	@Autowired
	private UserService userService;
	
	/**
	@RequestMapping("/user") //mapping映射url路径
	public ResponseResult index() {
		List<LabUser> users = null;
    	
    	if (users==null || users.isEmpty()) 
    		return new ResponseResult("0","sucessful",null);
    	else 
    		return new ResponseResult("1","failed",users);
    }
	 */
	/**
	@GetMapping("/{id}")
    public Map<String,String> getPerson(@PathVariable Long id) {
    	Map<String,String> returnMap= new HashMap<>();
    	returnMap.put("test", "test");
    	return returnMap;
    }
	 */
	
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseResult<LabUser> checkUser(@RequestBody LabUser user) {
    	
    	List<LabUser> users = userService.findUser(user);
    	
    	if (users==null || users.isEmpty()) 
    		return new ResponseResult<LabUser>("0","failed",users);
    	else 
    		return new ResponseResult<LabUser>("1","sucessful",null);
    	
        
    }
}