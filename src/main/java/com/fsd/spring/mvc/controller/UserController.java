package com.fsd.spring.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fsd.spring.mvc.core.base.ResponseResult;
import com.fsd.spring.mvc.entity.LabUser;
import com.fsd.spring.mvc.service.UserService;

@RestController
@RequestMapping({"/user"})
class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping
	public ResponseResult<LabUser> updateUser(@RequestBody LabUser user) {		
        return userService.update(user);
    }

	@GetMapping("/{id}")	
    public Map<String,String> getUser(@PathVariable Long id) {
    	Map<String,String> returnMap= new HashMap<>();
    	returnMap.put("test", "test");
    	return returnMap;
    }

	/*
	 * @PostMapping
	 * 
	 * @ResponseStatus(HttpStatus.CREATED) public void add(@RequestBody
	 * Map<String,String> person) { // ... }
	 */
}