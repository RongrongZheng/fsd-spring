package com.fsd.spring.mvc.core.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.SessionAttributes;

@Component
@SessionAttributes("userSessionInfo")
public class MyUserDetailService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
//		List<GrantedAuthority> authorities= new ArrayList<>();
//		authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        String passwordEncode = passwordEncoder.encode("Passw0rd");
		User user=new User("test1",passwordEncode,AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
		
		return user;
	}

}
