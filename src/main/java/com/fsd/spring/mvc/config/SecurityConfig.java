package com.fsd.spring.mvc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsUtils;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
    	   .authorizeRequests()
    	   // 所有 / 的所有请求 都放行
    	   .requestMatchers(CorsUtils::isPreFlightRequest).permitAll() ;//对preflight放行
    	  
    	 
    }

}