package com.fsd.spring.mvc.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fsd.spring.mvc.core.base.ResponseResult;
import com.fsd.spring.mvc.dao.UserDao;
import com.fsd.spring.mvc.entity.LabUser;

@Service
public class UserService {
	
	@Autowired
	private UserDao userDao;
	

	public ResponseResult<LabUser> update(LabUser user) {
		
		List<LabUser> users=userDao.findById(String.valueOf(user.getId()));
		if(users==null || users.isEmpty() )
			userDao.insert(user);
		else
			userDao.update(user);
		List<LabUser> labusers= new ArrayList<LabUser>();
		users.add(user);
    	return new ResponseResult<LabUser>("1","sucessful",labusers);
		
	}
	
	public List<LabUser> findUser(LabUser user) {
		
		List<LabUser> labusers=userDao.findByName(user.getUsername());
		
		if(labusers == null || labusers.isEmpty()) return null;
		
		if(user.getPassword().equals(labusers.get(0).getPassword())) return labusers;
		
		return null;
		
	}

}
